#include "backend.hpp"

#include <QGuiApplication>

const QString SERIAL { QStringLiteral("serial") };
const QString NAME { QStringLiteral("name") };

QString Backend::slug(const QScreen* display) const {
    if(display) {
        QString serial = display->serialNumber();
        // Being able to use the serial guarantees unique identification across connections
        if(!serial.isEmpty()) {
            return SERIAL + DELIM + serial + DELIM + display->model() + DELIM + display->manufacturer();
        } else {
            // This still uniquely identifies, but we can lose the screen if the attached output name changes
            return NAME + DELIM + display->name() + DELIM + display->model() + DELIM + display->manufacturer();
        }
    }
    return {};
}

QScreen* Backend::deslug(QString slug) const {
    if(slug.startsWith(SERIAL)) {
        QStringList sep = slug.split(DELIM);
        if(sep.length() == 4) {
            for (QScreen* scr : QGuiApplication::screens()) {
                if(scr->serialNumber() == sep[1] &&
                   scr->model() == sep[2] &&
                   scr->manufacturer() == sep[3])
                    return scr;
            }
        }
    } else if(slug.startsWith(NAME)) {
        QStringList sep = slug.split(DELIM);
        if(sep.length() == 4) {
            for(QScreen* scr : QGuiApplication::screens()) {
                if(scr->name() == sep[1] &&
                   scr->model() == sep[2] &&
                   scr->manufacturer() == sep[3])
                    return scr;
            }
        }
    }
    qCDebug(KCM_TABLET) << "Deslug didn't find a display for:" << slug;
    return nullptr;
}

