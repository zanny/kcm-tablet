#include "kwin_backend.hpp"

#include "kwin_tablet.hpp"

#include <KConfigGroup>

#include <QDBusArgument>
#include <QDBusMetaType>
#include <QMatrix4x4>

QDBusArgument &operator<<(QDBusArgument &argument, const QMatrix4x4 &matrix)
{
    argument.beginArray(qMetaTypeId<double>());
    for(quint8 row = 0; row < 4; ++row)
        for(quint8 col = 0; col < 4; ++col)
            argument << matrix(row, col);
    argument.endArray();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, QMatrix4x4 &matrix)
{
    argument.beginArray();
    for(quint8 row = 0; row < 4; ++row)
        for(quint8 col = 0; col < 4; ++col)
        {
            double val;
            argument >> val;
            matrix(row, col) = val;
        }
    argument.endArray();
    return argument;
}

#define MANAGER QStringLiteral("org.kde.KWin.InputDeviceManager")

KWinBackend::KWinBackend() :
    kwin(QDBusInterface(KWinTablet::SERVICE, KWinTablet::PATH, MANAGER,
                        QDBusConnection::sessionBus(),
                        this)) {
    qDBusRegisterMetaType<QMatrix4x4>();

    kwin.connection().connect(KWinTablet::SERVICE, KWinTablet::PATH, MANAGER,
                              QStringLiteral("deviceAdded"),
                              this,
                              SLOT(add(QString)));
    kwin.connection().connect(KWinTablet::SERVICE, KWinTablet::PATH, MANAGER,
                              QStringLiteral("deviceRemoved"),
                              this,
                              SLOT(remove(QString)));

    QStringList sysNames = kwin.property("devicesSysNames").toStringList();
    for(auto& name: sysNames)
        add(name);

    if(!orphans.empty())
        for(auto& orphan : orphans)
            qCDebug(KCM_TABLET) << "Orphan pad with no tool at startup: " << orphan.sysName;
}

void KWinBackend::defaults(Tablet& tablet) const {
    tablet.setDisplay(nullptr);
    tablet.setLeft(tablet.defaultLeft);
    // Are there tablets that have a primary orientation that isn't landscape?
    tablet.setRotation(Tablet::Rotation::Zero);

    auto& ktab = static_cast<KWinTablet&>(tablet);
    auto matrix = ktab.bus().property("defaultCalibrationMatrix").value<QMatrix4x4>();
    tablet.setCalibration({ matrix(0,0), matrix(0, 2), matrix(1, 1), matrix(1,2) });
}

void KWinBackend::load(Tablet& tablet) const {
    auto& ktab = static_cast<KWinTablet&>(tablet);
    auto screen = ktab.bus().property(KWinTablet::SCREEN).toString();
    tablet.findDisplay(screen);

    if(tablet.nativeLeft)
        tablet.setLeft(ktab.bus().property(KWinTablet::LEFTHANDED).toBool());
    else {
        KConfigGroup group = cfg->group(tablet.slug);
        tablet.setLeft(group.readEntry(LEFT, false));
    }

    bool ok;
    auto orientation = static_cast<Qt::ScreenOrientation>(ktab.bus().property(KWinTablet::ORIENTATION).toInt(&ok));
    if (!ok) {
        qCWarning(KCM_TABLET) << "Tablet" << &ktab << "couldn't read orientation property";
        return;
    }
    switch(orientation) {
        // Do any tablets have a non-landscape primary orientation?
        case Qt::PrimaryOrientation:
        case Qt::LandscapeOrientation:
            tablet.setRotation(Tablet::Rotation::Zero);
            break;
        case Qt::PortraitOrientation:
            tablet.setRotation(Tablet::Rotation::Ninety);
            break;
        case Qt::InvertedLandscapeOrientation:
            tablet.setRotation(Tablet::Rotation::OneEighty);
            break;
        case Qt::InvertedPortraitOrientation:
            tablet.setRotation(Tablet::Rotation::TwoSeventy);
            break;
    }

    auto calibration = ktab.bus().property(KWinTablet::CALIBRATION).value<QMatrix4x4>();
    tablet.setCalibration(CaliVars { calibration(0, 0), calibration(0, 2),
                                  calibration(1, 1), calibration(1, 2) });
    tablet.setActive();
}

void KWinBackend::save(Tablet& tablet) const {
    auto& ktab = static_cast<KWinTablet&>(tablet);

    if(tablet.display() && tablet.display() != tablet.activeDisplay())
        if(ktab.bus().setProperty(KWinTablet::SCREEN, tablet.display()->name()))
            tablet.setActiveDisplay();

    if(ktab.left() != ktab.activeLeft()) {
        if(ktab.nativeLeft) {
            if(ktab.bus().setProperty(KWinTablet::LEFTHANDED,ktab.left()))
                ktab.setActiveLeft();
            else
                qCWarning(KCM_TABLET) << "Failed to set left handed prop on" << &ktab;
        } else {
            KConfigGroup group = cfg->group(ktab.slug);
            if(ktab.left())
                group.writeEntry(LEFT, true);
            else
                group.deleteEntry(LEFT);
            ktab.setActiveLeft();
        }
    }

    if(ktab.rotation() != ktab.activeRotation()) {
        // We do assume landscape oriented tablets, if that is
        // even an invalid assumption we need to check dimensions.
        int orientation = Qt::LandscapeOrientation;
        switch(ktab.rotation()) {
            case Tablet::Rotation::Zero:
                break;
            case Tablet::Rotation::Ninety:
                orientation = Qt::PortraitOrientation;
                break;
            case Tablet::Rotation::OneEighty:
                orientation = Qt::InvertedLandscapeOrientation;
                break;
            case Tablet::Rotation::TwoSeventy:
                orientation = Qt::InvertedPortraitOrientation;
                break;
        }
        if(ktab.bus().setProperty(KWinTablet::ORIENTATION, orientation))
            ktab.setActiveRotation();
        else
            qCWarning(KCM_TABLET) << "Failed to set rotation prop on" << &ktab;
    }

    if(ktab.calibration() != ktab.activeCalibration()) {
        auto matrix = tablet.calibrationMatrix();
        if(!ktab.nativeLeft && ktab.left())
            matrix = matrix * REFLECTION;

        QMatrix4x4 calibration;
        for(uchar i = 0; i < 3; ++i)
            for(uchar j = 0; j < 3; ++j)
                calibration(i,j) = matrix(i,j);
        if(ktab.bus().setProperty(KWinTablet::CALIBRATION, calibration))
            ktab.setActiveCalibration();
        else
            qCWarning(KCM_TABLET) << "Falied to set calibration prop on" << &ktab;
    }
}

void KWinBackend::add(const QString& sysName) {
    // Check if we already have this tablet
    for(const Tablet* tablet: m_tablets) {
        auto& kt = static_cast<const KWinTablet&>(*tablet);
        if(kt.sysName == sysName || kt.padSysName == sysName)
            return;
    }

    // ??? parenting this to tablet means you don't need to track ownership but
    // if we don't make a tablet having to manually delete is so 2003
    auto dbus = std::make_unique<QDBusInterface>(KWinTablet::SERVICE, KWinTablet::PATH + '/' + sysName, KWinTablet::INTERFACE);

    if(!dbus->isValid()) {
        qCWarning(KCM_TABLET) << "Tablet dbus interface for" << sysName << "is invalid?";
        return;
    }

    bool pad = dbus->property("tabletPad").toBool();
    // If neither a pad nor a tool
    if(!pad && !dbus->property("tabletTool").toBool())
        return;

    QString name = dbus->property("name").toString();
    bool ok;
    uint vendor = dbus->property("vendor").toUInt(&ok);
    if(!ok)
        qCWarning(KCM_TABLET) << "Tablet device" << sysName << "doesn't have a vendor id?";
    if(vendor > std::numeric_limits<quint16>::max()) {
        qCCritical(KCM_TABLET) << "Reported vendor id" << vendor << "is larger than the 16 bit limit of USB spec?";
        vendor = 0;
    }
    uint product = dbus->property("product").toUInt(&ok);
    if(!ok)
        qCWarning(KCM_TABLET) << "Tablet device" << sysName << "doesn't have a usb product id?";
    if(product > std::numeric_limits<quint16>::max()) {
        qCCritical(KCM_TABLET) << "Reported product id" << product << "is larger than the 16 bit limit of USB spec?";
        product = 0;
    }

    // All pads can do is try to find the parent tool or wait for it to show up
    if(pad) {
        qCInfo(KCM_TABLET) << "Found pad" << name << sysName << vendor << product;
        bool found = false;
        for(Tablet * tab : m_tablets) {
            auto ktab = static_cast<KWinTablet *>(tab);
            if(ktab->padSysName.isEmpty() && ktab->vendor == vendor && ktab->product == product) {
                qCDebug(KCM_TABLET) << "Matched pad" << sysName << "with tool" << ktab->sysName;
                ktab->padSysName = sysName;
                found = true;
                break;
            }
        }
        if(!found)
            orphans.push_back({sysName, static_cast<quint16>(vendor), static_cast<quint16>(product)});

        return;
    }

    bool supportsLeftHanded = dbus->property("supportsLeftHanded").toBool();
    bool leftHandedEnabledByDefault = dbus->property("leftHandedEnabledByDefault").toBool();
    if(!dbus->property("supportsCalibrationMatrix").toBool()) {
        qCCritical(KCM_TABLET) << "Tablet device" << sysName << "doesn't support a calibration matrix?";
        return;
    }

    auto tablet = new KWinTablet(std::move(dbus),
                                name, sysName,
                                vendor, product,
                                supportsLeftHanded, leftHandedEnabledByDefault,
                                this);
    // Because pads and tools can be in any order we cannot assume we have either
    for(auto it = orphans.begin(); it != orphans.end(); ++it) {
        if(vendor == it->vendor && product == it->product) {
            qCDebug(KCM_TABLET) << "Matched tool" << sysName << "with pad" << it->sysName;
            tablet->padSysName = it->sysName;
            orphans.erase(it);
            break;
        }
    }
    qCInfo(KCM_TABLET) << "Found tool" << name << sysName << vendor << product;
    m_tablets << tablet;
    emit added(*tablet);
}

void KWinBackend::remove(const QString& name) {
    for(int i = 0; i < m_tablets.size(); ++i) {
        auto ktab = static_cast<KWinTablet*>(m_tablets[i]);
        if(ktab->sysName == name) {
            m_tablets.removeAt(i);
            emit removed(*ktab);
            // Can this misbehave with the signal?
            ktab->deleteLater();
        }
    }
}
