#pragma once

#include "tablet.hpp"

#include <QDBusInterface>

class KWinTablet : public Tablet {
    Q_PROPERTY(QString sysName MEMBER sysName CONSTANT)
    Q_PROPERTY(bool supportsLeftHanded MEMBER supportsLeftHanded CONSTANT)
    Q_PROPERTY(bool leftHandedEnabledByDefault MEMBER leftHandedEnabledByDefault CONSTANT)

    std::unique_ptr<QDBusInterface> dbus;

public:
    KWinTablet(std::unique_ptr<QDBusInterface>&&, QString, QString, quint16, quint16, bool, bool, QObject * = nullptr);

    static inline const QString SERVICE { QStringLiteral("org.kde.KWin") },
                                PATH { QStringLiteral("/org/kde/KWin/InputDevice") },
                                INTERFACE { QStringLiteral("org.kde.KWin.InputDevice") };
    static inline const char * const LEFTHANDED {"leftHanded"},
                             * const CALIBRATION {"calibrationMatrix"},
                             * const ORIENTATION {"orientation"},
                             * const SCREEN {"screen"};

    const QString sysName;

    QString padSysName;

    QDBusInterface& bus() { return *dbus; }
};
