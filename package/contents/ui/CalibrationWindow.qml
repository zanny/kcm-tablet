import QtQuick 2.13
import QtQuick.Window 2.13
import QtQuick.Controls 2.13 as Controls
import org.kde.kirigami 2.5 as Kirigami
import calibration 1.0

Window {
    id: root
    color: palette.window
    Controls.Label {
        anchors.centerIn: parent
        text: i18n("Press and hold pen tip directly in center of targets. Double tap anywhere else to close.")
        font.weight: Font.Bold
        font.pointSize: Math.round(Kirigami.Units.fontMetrics.font.pointSize * 1.25)
    }
    MouseArea {
        anchors.fill: parent
        onDoubleClicked: root.close()
    }

    property real block: Math.min(width, height) / 20
    property var calibration: kcm.calibration(Qt.rect(block * 2, block * 2, width - block * 4, height - block * 4))

    Calibrator {
        corner: Calibration.TopLeft
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.margins: block
    }
    Calibrator {
        corner: Calibration.TopRight
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: block
    }
    Calibrator {
        corner: Calibration.BottomLeft
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: block
    }
    Calibrator {
        corner: Calibration.BottomRight
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: block
    }

    Connections {
        target: calibration
        function onFinalized() { close() }
    }
}
