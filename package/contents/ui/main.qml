import QtQuick 2.13
import QtQuick.Controls 2.13 as Controls
import QtQuick.Layouts 1.13
import org.kde.kirigami 2.5 as Kirigami
import org.kde.kcm 1.1 as KCM
import tablet 1.0

KCM.SimpleKCM {
    id: root
    implicitWidth: screenCols.width + 100

    property int maxButtons: {
        let n = Qt.MaxMouseButton;
        let digits = 0;
        while(n) {
            digits++;
            n = n >> 1;
        }
        return digits;
    }
    property bool wayland: false

    SystemPalette {
        id: palette
    }
    Controls.Label {
        anchors.centerIn: parent
        horizontalAlignment: Text.AlignHCenter
        text: i18n("No pen tablets detected! Make sure they show up in libinput list-devices with the capability tablet.")
        visible: kcm.current === null
        width: parent.width * .8
        wrapMode: Text.WordWrap
    }
    ColumnLayout {
        visible: kcm.current !== null
        Controls.TabBar {
            visible: kcm.tablets.length > 1
            enabled: visible
            Repeater {
                model: kcm.tablets
                Controls.TabButton {
                    icon.name: "input-tablet"
                    text: modelData.name
                    onClicked: kcm.current = modelData
                }
            }
        }
        FocusScope {
            implicitWidth: virtualScreen.width
            implicitHeight: screenCols.height
            Layout.alignment: Qt.AlignHCenter
            ColumnLayout {
                id: screenCols

                Controls.Label {
                    text: root.wayland ? i18n("Screen Mapping") : i18n("Screen Mapping (whole box is all screens)")
                    Layout.leftMargin: Kirigami.Units.largeSpacing
                }
                Rectangle {
                    id: virtualScreen
                    focus: kcm.current.display == null

                    // Nothing on Screen tells us the total virtual desktop area
                    property size area: {
                        if(root.wayland) {
                            let scr = Qt.applications.screens[0]
                            let w = scr.desktopAvailableWidth
                            let h = scr.desktopAvailableHeight
                            return Qt.size(w, h)
                        }
                        return kcm.area()
                    }

                    width: height * area.width / area.height
                    height: 400
                    radius: 4
                    color: focus ? palette.light : palette.dark
                    Behavior on color {
                            PropertyAnimation {
                                duration: Kirigami.Units.shortDuration
                            }
                    }
                    border {
                        color: focus ? palette.highlight : palette.shadow
                        width: 5
                        Behavior on color {
                            PropertyAnimation {
                                duration: Kirigami.Units.shortDuration
                            }
                        }
                    }

                    property real scalar: {
                        let relative = Qt.size(.9 * width / area.width,  .9 * height / area.height);
                        return relative.width < relative.height ? relative.width : relative.height;
                    }

                    MouseArea {
                        enabled: !root.wayland
                        anchors.fill: parent
                        onClicked: kcm.current.display = null
                    }

                    Repeater {
                        model: Qt.application.screens
                        Rectangle {
                            width: (root.wayland ? 1 : devicePixelRatio) * modelData.width * virtualScreen.scalar
                            height: (root.wayland ? 1 : devicePixelRatio) * modelData.height * virtualScreen.scalar
                            x: parent.width * .05 + virtualX * virtualScreen.scalar
                            y: parent.height * .05 + virtualY * virtualScreen.scalar
                            color: focus ? palette.light : palette.window
                            Behavior on color {
                                PropertyAnimation {
                                    duration: Kirigami.Units.shortDuration
                                }
                            }
                            border {
                                color: focus ? palette.highlight : palette.shadow
                                width: 5
                                Behavior on color {
                                    PropertyAnimation {
                                        duration: Kirigami.Units.shortDuration
                                    }
                                }
                            }
                            Controls.Label {
                                anchors.centerIn: parent
                                text: modelData.name + "\n" + virtualX + ", " + virtualY + " " + modelData.width * modelData.devicePixelRatio + "x" + modelData.height * modelData.devicePixelRatio
                            }
                            MouseArea {
                                anchors.fill: parent
                                onClicked: kcm.current.displayName = modelData.name
                            }
                            focus: kcm.current.displayName == modelData.name
                        }
                    }
                }
                Controls.Label {
                    id: screenText
                    text: i18n("Selected screen: ") + (kcm.current.display === null ? i18n("All Screens") : kcm.current.display.name)
                    Layout.leftMargin: Kirigami.Units.largeSpacing
                }
            }
        }

        Kirigami.FormLayout {
            Controls.CheckBox {
                text: i18n("Left Handed Mode")
                checked: kcm.current.left
                onClicked: kcm.current.toggleLeft()
                enabled: !root.wayland || kcm.current.nativeLeft
            }

            RowLayout {
                Kirigami.FormData.label: i18n("Orientation:")
                RotationButton {
                    value: Tablet.Rotation.Zero
                    tooltip: i18n("No Rotation")
                }
                RotationButton {
                    value: Tablet.Rotation.Ninety
                    tooltip: i18n("90° Clockwise")
                }
                RotationButton {
                    value: Tablet.Rotation.OneEighty
                    tooltip: i18n("Upside Down")
                }
                RotationButton {
                    value: Tablet.Rotation.TwoSeventy
                    tooltip: i18n("90° Counterclockwise")
                }
            }

            Component {
                id: calibrator
                CalibrationWindow {}
            }
            Kirigami.Separator {
                Kirigami.FormData.label: i18n("Calibration")
                Kirigami.FormData.isSection: true
            }
            Controls.Button {
                text: i18n("Calibrate")
                Layout.fillWidth: true
                Layout.preferredHeight: 40
                enabled: kcm.current.display !== null && kcm.current.activeDisplay === kcm.current.display && kcm.current.activeLeft === kcm.current.left && kcm.current.activeRotation === kcm.current.rotation
                onClicked: {
                    for(var i in Qt.application.screens) {
                        var screen = Qt.application.screens[i]
                        if(screen.name === kcm.current.display.name) {
                            break
                        }
                    }
                    let geo = kcm.current.display.geometry
                    let ratio = root.wayland ? 1 : kcm.current.display.devicePixelRatio
                    print("Creating calibration window @", geo, ratio)
                    let calibration = calibrator.createObject(parent, {screen: screen, x: geo.x, y: geo.y, width: geo.width * ratio, height: geo.height * ratio})
                    calibration.showFullScreen();
                }
            }

            GridLayout {
                columns: 2
                property real screenWidth: kcm.current.display === null ? virtualScreen.area.width : kcm.current.display.size.width * kcm.current.display.devicePixelRatio
                property real screenHeight: kcm.current.display === null ? virtualScreen.area.height: kcm.current.display.size.height * kcm.current.display.devicePixelRatio

                Controls.Label {
                    text: i18n("Horizontal Offset") + ": " + Math.round(kcm.current.calibration.x * parent.screenWidth)
                }
                Controls.Label {
                    text: i18n("Vertical Offset") + ": " + Math.round(kcm.current.calibration.y * parent.screenHeight)
                }
                Controls.Label {
                    text: i18n("Width") + ": " + Math.round(kcm.current.calibration.width * parent.screenWidth)
                }
                Controls.Label {
                    text: i18n("Height") + ": " + Math.round(kcm.current.calibration.height * parent.screenHeight)
                }
            }

            Kirigami.Separator {
                visible: kcm.current.buttons.length
                Kirigami.FormData.label: i18n("Button Map")
                Kirigami.FormData.isSection: true
            }
            Repeater {
                // Make the button collection on the current tablet
                model: kcm.current.buttons

                Controls.SpinBox {
                    Kirigami.FormData.label: i18n("Button") + ' ' + (index + 1) 
                    to: root.maxButtons
                    value: modelData
                    editable: true
                    wrap: true
                    Layout.fillWidth: true

                    onValueModified: kcm.current.setButton(index, value)

                    // This exists because invoking setButtons per button changed 
                    // causes a lot of undefined reference errors.
                    Connections {
                        target: kcm.current
                        function onButtonChanged(i, v) {
                            if(i === index) {
                                value = v
                            }
                        }
                    }
                }
            }
        }
    }
}
