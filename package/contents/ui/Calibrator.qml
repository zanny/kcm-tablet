import QtQuick 2.13
import QtQuick.Shapes 1.13
import org.kde.kirigami 2.5 as Kirigami

Item {
    id: root
    width: block * 2
    height: width
    property int corner
    property color guts: palette.highlight
    property int minor: 2
    Kirigami.Theme.colorSet: Kirigami.Theme.Button

    Rectangle {
        id: outer
        anchors.centerIn: parent
        width: parent.width * .5
        height: width
        radius: width / 2
        color: "transparent"
        border.width: minor
        border.color: parent.guts
    }
    Rectangle {
        id: inner
        anchors.centerIn: parent
        width: parent.width * .25
        height: width
        radius: width / 2
        color: "transparent"
        border.width: minor
        border.color: parent.guts
    }
    Rectangle {
        id: horiz
        anchors.centerIn: parent
        width: parent.width * .75
        height: minor
        radius: 2
        color: parent.guts
    }
    Rectangle {
        id: vert
        anchors.centerIn: parent
        width: minor
        height: parent.height * .75
        radius: 2
        color: parent.guts
    }
    Rectangle {
        id: center
        anchors.centerIn: parent
        width: minor * 3
        height: width
        radius: width / 2
        color: Kirigami.Theme.negativeTextColor
        border.width: minor / 2
        border.color: Kirigami.Theme.neutralTextColor
    }
    Shape {
        anchors.fill: parent
        ShapePath {
            id: path
            fillColor: "transparent"
            strokeWidth: 20
            strokeColor: palette.highlight
            capStyle: ShapePath.RoundCap
            PathAngleArc {
                id: arc
                centerX: root.width / 2
                centerY: centerX
                radiusX: centerX
                radiusY: radiusX
                startAngle: 270
                sweepAngle: -360
            }
        }
    }

    Timer {
        id: start
        // Time after pen down to start recording points
        interval: 200
        onTriggered: calibration.start(corner)
    }
    // longPressed doesn't work if the tap causes a focus change,
    // thus two timers
    Timer {
        id: finish
        // How long to record points for
        interval: 3000
        onTriggered: {
            root.state = "COMPLETE"
            calibration.finish()
        }
    }
    TapHandler {
        id: handler
        acceptedDevices: PointerDevice.Stylus
        acceptedPointerTypes: PointerDevice.Pen
        gesturePolicy: TapHandler.WithinBounds

        onPressedChanged: {
            if(pressed) {
                root.state = "ACTIVE"
            } else if(root.state != "COMPLETE") {
                root.state = ""
                calibration.clear(corner)
            }
        }
        onPointChanged: calibration.commit(point.scenePosition)
    }

    MouseArea {
        anchors.fill: parent
        enabled: false
        cursorShape: Qt.BlankCursor
    }

    states: [
        State {
            name: "ACTIVE"
            PropertyChanges {
                target: arc
                sweepAngle: 0
                radiusX: centerX / 2
            }
            PropertyChanges {
                target: path
                strokeWidth: 15
                // TODO this doesn't work for some reason?
                strokeColor: Kirigami.Theme.positiveTextColor
            }
            PropertyChanges {
                target: start
                running: true
            }
            PropertyChanges {
                target: finish
                running: true
            }
        },
        State {
            name: "COMPLETE"
            PropertyChanges {
                target: arc
                sweepAngle: 0
                radiusX: centerX / 2
            }
            PropertyChanges {
                target: path
                strokeWidth: 0
            }
            PropertyChanges {
                target: center
                color: Kirigami.Theme.disabledTextColor
                border.color: Kirigami.Theme.disabledTextColor
            }
            PropertyChanges {
                target: root
                guts: Kirigami.Theme.disabledTextColor
            }
            PropertyChanges {
                target: handler
                enabled: false
            }
        }
    ]

    transitions: [
        Transition {
            ColorAnimation {
                targets: [outer, inner, horiz, vert, center, center.border]
                properties: "color"
            }
            ColorAnimation {
                target: path
                properties: "strokeColor"
            }
        },
        Transition {
            to: "ACTIVE"
            NumberAnimation {
                target: arc
                properties: "radiusX,sweepAngle"
                duration: finish.interval
            }
            NumberAnimation {
                target: path
                properties: "strokeWidth"
                duration: finish.interval
            }
            ColorAnimation {
                target: path
                properties: "strokeColor"
                duration: finish.interval
            }
        },
        Transition {
            from: "ACTIVE"
            NumberAnimation {
                target: arc
                properties: "radiusX, sweepAngle"
            }
            ColorAnimation {
                target: path
                properties: "strokeColor"
            }
        }
    ]
}

