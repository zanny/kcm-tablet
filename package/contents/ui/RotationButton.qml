import QtQuick 2.13
import QtQuick.Controls 2.13 as Controls
import org.kde.kirigami 2.5 as Kirigami
import org.kde.kcm 1.1 as KCM

Controls.Button {
    property int value
    property string tooltip

    checked: kcm.current.rotation === value
    onClicked: kcm.current.rotation = value

    Controls.ToolTip {
        text: tooltip
    }

    contentItem: Kirigami.Icon {
        source: "view-preview"
        rotation: value
    }

    implicitWidth: contentItem.implicitWidth + 2 * Kirigami.Units.smallSpacing
    implicitHeight: contentItem.implicitHeight + 2 * Kirigami.Units.smallSpacing
}
