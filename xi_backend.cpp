#include "xi_backend.hpp"

#include <KConfigGroup>

#include <regex>

#include <X11/Xatom.h>
#include <X11/extensions/XInput.h>
#include <X11/extensions/XInput2.h>
#include <xorg/libinput-properties.h>
#include <xorg/xserver-properties.h>
#include <xcb/xproto.h>

XiBackend::XiBackend(const std::unordered_map<std::string, xcb_atom_t> atoms): Backend(),
    dpy(QX11Info::display()),
    float_(atoms.at(XATOM_FLOAT)),
    xi_tablet(atoms.at(XI_TABLET)),
    id(atoms.at(XI_PROP_PRODUCT_ID)),
    pad(atoms.at("PAD")),
    pen(atoms.at("STYLUS")),
    calibrationMatrixDefault(atoms.at(LIBINPUT_PROP_CALIBRATION_DEFAULT)),
    leftHanded(atoms.at(LIBINPUT_PROP_LEFT_HANDED)),
    leftHandedDefault(atoms.at(LIBINPUT_PROP_LEFT_HANDED_DEFAULT)),
    calibrationMatrix(atoms.at(LIBINPUT_PROP_CALIBRATION)) {
    static const std::regex tab_re("^(.+) Pen$");
    static const std::regex pad_re("^(.+) Pad$");
    static const std::regex pen_re("^(.+) Pen Pen \\((\\d+)\\)$");

    int ndevices;
    XDeviceInfo *info = XListInputDevices(dpy, &ndevices);

    if(!info) return;

    // Key for the tablet info map - the X assigned base name and the vid:pid
    struct ident {
        std::string name;
        uint16_t vendor;
        uint16_t product;

        bool operator==(const ident& rhs) const noexcept {
            return name == rhs.name && vendor == rhs.vendor && product == rhs.product;
        }
    };

    struct hash {
        size_t operator() (const ident& id) const {
            return std::hash<std::string>{}(id.name + std::to_string(id.vendor) + std::to_string(id.product));
        }
    };

    // Value type of the tablet data map, representing X Device ids for each
    // part of the tablet that span multiple devices
    struct ids {
        XID tablet;
        XID pad;
        short nbtns;
        std::unordered_map<int, XID> pens;
    };

    std::unordered_map<ident, ids, hash> tablets;
    std::smatch matches;
    std::optional<std::pair<uint16_t, uint16_t>> vidpid;

    for(int i = 0; i < ndevices; ++i) {
        XDeviceInfo *dev = info + i;
        std::string name(dev->name);
        if(dev->type == xi_tablet &&
            dev->use == IsXExtensionDevice &&
            std::regex_match(name, matches, tab_re) &&
            (vidpid = get_vidpid(dev->id))) {
                ident key { matches[1], vidpid->first, vidpid->second };
                ids& tab = tablets[key];
                tab.tablet = dev->id;
                qCInfo(KCM_TABLET) << "Found tablet:" << key.name.data() << "id:" << tab.tablet;
        }
        else if(dev->type == pad &&
                      dev->use == IsXExtensionPointer &&
                      std::regex_match(name, matches, pad_re) &&
                      (vidpid = get_vidpid(dev->id))) {
            ident key { matches[1], vidpid->first, vidpid->second };
            ids& tab = tablets[key];
            XAnyClassPtr ip = dev->inputclassinfo;
            short buttons = 0;
            for(int i = 0; i < dev->num_classes; i++) {
                if(ip->c_class == ButtonClass) {
                    short btns = ((XButtonInfoPtr)ip)->num_buttons;
                    if(btns > buttons)
                        buttons = btns;
                }
                ip = ip + ip->length;
            }
            if(buttons) {
                tab.pad = dev->id;
                // X buttons 4-7 are reserved mouse specials that show up but don't do anything
                if(buttons > 3) buttons -= 4;
                tab.nbtns = buttons;
            }
            qCInfo(KCM_TABLET) << "Found pad:" << key.name.data() << "id:" << tab.pad << "# buttons:" << buttons;
        }

        else if(dev->type == pen &&
                      dev->use == IsXExtensionPointer &&
                      std::regex_match(name, matches, pen_re) &&
                      (vidpid = get_vidpid(dev->id))) {
            ident key { matches[1], vidpid->first, vidpid->second };
            int num = stoi(matches[2]);
            ids& tab = tablets[key];
            tab.pens[num]= dev->id;
            qCInfo(KCM_TABLET) << "Found pen:" << key.name.data() << "id:" << tab.pens[num];
        }
    }

    for(std::pair<ident, ids> params : tablets) {
        QString name = QString::fromStdString(params.first.name);
        // pads and pens are optional - the tablet itself is not
        XID tabid = params.second.tablet;
        if(tabid == 0) {
            qCWarning(KCM_TABLET) << "Pad / pens without tablet! : " << name;
            continue;
        }
        bool nativeLeft = false,
             currentLeft = false,
             defaultLeft = false;
        const ulong size = 1;
        // Could xcb these for faster tab iteration
        auto result = getProp<uchar>(tabid, leftHanded, XA_INTEGER, 8, size);
        // We only care if the leftHanded prop exists
        if(result.size() == size) {
            nativeLeft = true;
            currentLeft = result[0];
            result = getProp<uchar>(tabid, leftHandedDefault, XA_INTEGER, 8, size);
            if(result.size() == size && result[0])
                defaultLeft = true;
        }

        XiTablet *tablet = new XiTablet(name, params.first.vendor, params.first.product,
                                        nativeLeft, currentLeft, defaultLeft, tabid, params.second.pad,
                                        params.second.nbtns, this);
        tablet->pens = params.second.pens;
        m_tablets.append(tablet);
    }

    XFreeDeviceList(info);
}

template<typename T> std::vector<T> XiBackend::getProp(
    XID dev, Atom prop, Atom type, int format, ulong num)  const {
    Atom type_return;
    int format_return;
    ulong num_items_return, bytes_after_return;
    uchar *data;

    Status status = XIGetProperty(dpy, dev, prop, 0, num, false, type, &type_return, 
                                  &format_return, &num_items_return, &bytes_after_return, &data);
    if(status != Success) {
        std::array<char, 255> buffer;
        int e = XGetErrorText(dpy, status, buffer.data(), buffer.size());
        qCWarning(KCM_TABLET) << "Get property fail error #" << status << "message" << buffer.data() << " code " << e;
    } else if (type_return != type) {
        auto ret = XGetAtomName(dpy, type_return);
        auto exp = XGetAtomName(dpy, type);
        auto pn = XGetAtomName(dpy, prop);
        qCDebug(KCM_TABLET) << "Return atom type:" << ret << "doesn't match expected type:" << exp << "for prop:" << pn;
        XFree(ret);
        XFree(exp);
        XFree(pn);
    } else if (format_return != format)
        qCDebug(KCM_TABLET) << "Return format:" << format_return << "doesn't match expected format:" << format;
    else if (num_items_return != num)
        qCDebug(KCM_TABLET) << "Number of items returned:" << num_items_return << "num items expected:" << num;
    else if (bytes_after_return != 0)
        qCDebug(KCM_TABLET) << "Unexpected bytes in return:" << bytes_after_return;
    else
        return std::vector<T>(reinterpret_cast<T*>(data),
                              reinterpret_cast<T*>(data + sizeof(T) * num));
    return {};
}

// Given a X Device ID query XInput for its 32 bit usb identifier
std::optional<std::pair<uint16_t, uint16_t>> XiBackend::get_vidpid(XID dev) const {
    ulong size = 2;
    auto prop = getProp<int>(dev, id, XA_INTEGER, 32, size);
    if(prop.size() == size)
        return std::make_pair((uint16_t) prop[0], (uint16_t) prop[1]);
    return std::nullopt;
}

QString XiBackend::XiErrName(int res) const {
        QString name;
        int baddevice;
        BadDevice(dpy, baddevice);
        // Can't use switch b/c we can't const baddevice
        if(res == baddevice) {
            // Is QStringLiteral an optimization here? Esp since its the return?
            name = "Bad Device";
        } else if (res == BadMatch) {
            name = "Bad Match";
        } else if(res == BadValue) {
            name = "Bad Value";
        }
        return name;
}

std::unique_ptr<XiBackend> XiBackend::build() {
    // PAD and STYLUS defined in xf86libinput.c, no header to include for defs
    static const std::array names { XATOM_FLOAT, XI_TABLET, XI_PROP_PRODUCT_ID, "PAD", "STYLUS",
                                    LIBINPUT_PROP_CALIBRATION, LIBINPUT_PROP_CALIBRATION_DEFAULT,
                                    LIBINPUT_PROP_LEFT_HANDED, LIBINPUT_PROP_LEFT_HANDED_DEFAULT };
    std::unordered_map<std::string, xcb_intern_atom_cookie_t> cookies;
    std::unordered_map<std::string, xcb_atom_t> atoms;

    for(auto name : names) {
        cookies[name] = xcb_intern_atom(QX11Info::connection(), false, strlen(name), name);
    }
    for(auto name: names) {
        // Have to give xcb a raw pointer we need to manually manage
        xcb_generic_error_t *error;
        std::unique_ptr<xcb_intern_atom_reply_t> reply { xcb_intern_atom_reply(QX11Info::connection(), cookies[name], &error) };
        if(error) {
            std::array<char, 255> res;
            XGetErrorText(QX11Info::display(), error->error_code, res.data(), res.size());
            qCWarning(KCM_TABLET) << "Error interning atom" << name << "code:"
                                << error->error_code << "text?" << res.data();
            delete error;
        }
        if(reply) {
            if(reply->atom)
                atoms[name] = reply->atom;
            else
                qCWarning(KCM_TABLET) << "Atom missing & can't create?" << name;
        } else
            qCWarning(KCM_TABLET) << "No reply for intern atom" << name;
    }

    // Can't use make_unique because of private constructor
    return std::unique_ptr<XiBackend>(new XiBackend(atoms));
}

void XiBackend::defaults(Tablet& tablet) const {
    tablet.setDisplay(nullptr);
    tablet.setLeft(tablet.defaultLeft);
    // Are there tablets that have a primary orientation that isn't landscape?

    tablet.setRotation(Tablet::Rotation::Zero);

    XiTablet& xtab = dynamic_cast<XiTablet&>(tablet);
    const ulong csize = 9;
    auto prop = getProp<float>(xtab.tablet, calibrationMatrixDefault, float_, 32, csize);
    if(prop.size() == csize)
        tablet.setCalibration({prop[0], prop[2], prop[4], prop[5]});
    else
        tablet.setCalibration({});

    if(xtab.btnmod) {
        const QList<uint>& buttons = tablet.buttons();
        for(int i = 0; i < buttons.size(); i++)
            tablet.setButton(i, i + 1);
    }
}

void XiBackend::load(Tablet& tablet) const {
    KConfigGroup group = cfg->group(tablet.slug);
    const QString slug = group.readEntry(DISPLAY, "");
    tablet.setDisplay(deslug(slug));

    XiTablet& xtab = dynamic_cast<XiTablet&>(tablet);
    // If there is a display slug but the display wasn't found use previous matrix
    if(!slug.isEmpty() && !tablet.display()) {
        qCInfo(KCM_TABLET) << "Display unavailable, getting fallback calibration matrix";
        const QList<float> list = group.readEntry(FALLBACK, QList<float>());
        if(list.size() == 6) {
            xtab.fallback = QMatrix3x3(std::array { list[0], list[1], list[2],
                                                   list[3], list[4], list[5],
                                                   0.f, 0.f, 1.f }.data() );
        } else
            qCWarning(KCM_TABLET) << "Missing display and no previous matrix to use, config is busted!";
    }

    if(group.hasKey(LEFT))
        tablet.setLeft(group.readEntry(LEFT, false));
    else 
        tablet.setLeft(xtab.defaultLeft);

    static const int rotDefault = static_cast<int>(Tablet::Rotation::Zero);
    Tablet::Rotation rotation = static_cast<Tablet::Rotation>(group.readEntry(ROTATION, rotDefault));
    tablet.setRotation(rotation);

    bool found = false;
    if(group.hasKey(CALIBRATION)) {
        QString str = group.readEntry(CALIBRATION, "");
        auto cali = CaliVars::fromString(str);
        // does anyone ever change a default non-identity calibration matrix to identity?
        if(cali.isIdentity() && !str.isEmpty())
            group.deleteEntry(CALIBRATION);
        else {
            tablet.setCalibration(cali);
            found = true;
        }
    }
    if(!found) {
        const ulong size = 9;
        auto prop = getProp<float>(xtab.tablet, calibrationMatrixDefault, float_, 32, size);
        if(prop.size() == size)
            tablet.setCalibration({prop[0], prop[2], prop[4], prop[5]});
        else
            tablet.setCalibration({});
    }

    tablet.setActive();

    // Continue only if a pad device was found
    if(!xtab.pad) return;

    QList<uint> buttons = group.readEntry(BUTTONS, QList<uint>());
    if(buttons.length() == xtab.num_buttons) {
        tablet.setButtons(buttons);
        return;
    }

    QProcess xinput;
    xinput.start(QStringLiteral("xinput"),
                 QStringList() << QStringLiteral("get-button-map") << QString::number(xtab.pad));
    xinput.waitForFinished();
    QByteArray res = xinput.readAll();
    auto splits = res.split(' ');
    // There are 4 fixed X buttons that don't do anything plus the newline at the end
    if(splits.size() != xtab.num_buttons + 5) {
        qCWarning(KCM_TABLET) << "Unexpected response from get-button-map:" << res;
        return;
    }
    // Remove the four fake buttons
    for(int i = 0; i < 4; i++)
        splits.removeAt(3);
    // Remove the newline at the end
    splits.pop_back();
    QList<uint> map;
    bool ok;
    for(auto bytes: splits) {
        uint btn = bytes.toUInt(&ok);
        if(!ok) {
            qCWarning(KCM_TABLET) << "Malformed button value in get-button-map: " << bytes;
            return;
        }
        map.push_back(btn);
    }
    tablet.setButtons(map);
    xtab.btnmod = false;
}

void XiBackend::save(Tablet& tablet) const {
    XiTablet& xtab = dynamic_cast<XiTablet&>(tablet);

    if(!apply(xtab))
        return;

    KConfigGroup group = cfg->group(xtab.slug);
    if(tablet.display())
        group.writeEntry(DISPLAY, slug(xtab.display()));
    else if(group.hasKey(DISPLAY))
        group.deleteEntry(DISPLAY);

    if(tablet.left())
        group.writeEntry(LEFT, tablet.left());
    else if(group.hasKey(LEFT))
        group.deleteEntry(LEFT);

    auto& cali = tablet.calibration();
    if(!cali.isIdentity()) {
        group.writeEntry(CALIBRATION, cali.toString());
    } else if(group.hasKey(CALIBRATION))
        group.deleteEntry(CALIBRATION);

    if(xtab.rotation() != Tablet::Rotation::Zero) {
        group.writeEntry(ROTATION, static_cast<int>(xtab.rotation()));
    } else if(group.hasKey(ROTATION))
        group.deleteEntry(ROTATION);

    /* At kcminit display data may not be initialized so we dump
     * the final matrix in case we cannot rebuild it */
    auto matrix = xtab.compute();
    if(!matrix.isIdentity())
        group.writeEntry(FALLBACK, QList { float(matrix(0, 0)), float(matrix(0, 1)), float(matrix(0, 2)),
                                           float(matrix(1, 0)), float(matrix(1, 1)), float(matrix(1, 2)) });

    const QList<uint>& buttons = xtab.buttons();
    bool worthy = false;
    // If any buttons aren't identity mapped its worth saving
    for(int i = 0; i < buttons.length(); i++) {
        if(buttons[i] != uint(i) + 1) {
            worthy = true;
            break;
        }
    }
    if(worthy)
        group.writeEntry(BUTTONS, buttons);
    else if(group.hasKey(BUTTONS))
        group.deleteEntry(BUTTONS);
    group.sync();
}

bool XiBackend::apply(XiTablet& tablet) const {
    QMatrix3x3 matrix;

    if(!tablet.fallback.isIdentity() &&
        tablet.left() == tablet.activeLeft() &&
        tablet.calibration() == tablet.activeCalibration() &&
        tablet.rotation() == tablet.activeRotation()) {
        matrix = tablet.fallback;
    } else {
        auto mapped = tablet.mapped();
        tablet.rotate(mapped);

        if(tablet.left() != tablet.activeLeft()) {
            if(tablet.nativeLeft) {
                uchar data = tablet.left();
                // Doesn't return status, if this is problematic and
                // fails regularly can switch to xcb change property
                XIChangeProperty(dpy, tablet.tablet, leftHanded, XA_INTEGER, 8, XIPropModeReplace, &data, 1);
            } else if(tablet.left())
                mapped = mapped * REFLECTION;
        }
        matrix = XiTablet::downcast(mapped);
    }

    std::array<float, 9> data {matrix(0, 0), matrix(0, 1), matrix(0, 2),
                               matrix(1, 0), matrix(1, 1), matrix(1, 2),
                               0, 0, 1};
    XIChangeProperty(dpy, tablet.tablet, calibrationMatrix, float_, 32, XIPropModeReplace,
                     reinterpret_cast<uchar*>(data.data()), data.size());
    tablet.setActive();

    if(tablet.pad && tablet.btnmod) {
        QStringList args { QStringLiteral("set-button-map"), QString::number(tablet.pad) };
        for(auto btn : tablet.buttons())
            args << QString::number(btn);

        // Buttons 4-7 on Xinput devices are "reserved" and thus do nothing on pads
        if(tablet.buttons().size() > 3)
            for(int i = 0; i < 4; ++i)
                args.insert(5, "0");

        /* Before 60b10b7c I was trying to use XGet/SetDeviceButtonMapping. No matter what I tried it always returned
         * error code 14. Even copied verbatim the code that this xinput command uses to issue the same call. Except
         * calling out to XInput works so that is how its written now - its hugely inefficient and an external dependency
         * but until I can figure out how to make the native xlib / xcb versions cooperate its good enough.
         */
        QProcess xinput;
        xinput.start(QStringLiteral("xinput"), args);
        xinput.waitForFinished();
        QByteArray res = xinput.readAll();
        if(!res.isEmpty()) {
            qCWarning(KCM_TABLET) << "Setting button map failed, return was: " << res;
            return false;
        }
    }
    return true;
}
