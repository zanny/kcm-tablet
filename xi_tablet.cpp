#include <xi_tablet.hpp>


XiTablet::XiTablet(QString name, quint16 vendor, quint16 product, bool nativeLeft, bool currentLeft, bool defaultLeft,
                   XID tablet, XID pad, short num_buttons, QObject * parent):
                        Tablet(name, product, vendor, nativeLeft, defaultLeft, parent),
                        tablet(tablet), pad(pad), num_buttons(num_buttons), btnmod(false) {
    m_left = currentLeft;
    connect(this, &Tablet::buttonsChanged, this, [this] { btnmod = true; });
    connect(this, &Tablet::buttonChanged, this, [this] { btnmod = true; });
}

QMatrix3x3 XiTablet::downcast(const Matrix & matrix) {
    QMatrix3x3 result;
    for (uchar i = 0; i < 3; ++i)
        for(uchar j = 0; j < 3; ++j)
            result(i,j) = matrix(i,j);
    return result;
}

QMatrix3x3 XiTablet::compute() const {
    auto matrix = mapped();
    rotate(matrix);

    if(!nativeLeft && m_left)
        matrix = matrix * REFLECTION;

    return XiTablet::downcast(matrix);
}
