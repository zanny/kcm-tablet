#include "tablet.hpp"

#include <KLocalizedString>

#include "screen_monitor.hpp"

Tablet::Tablet(QString name, quint16 vendor, quint16 product,
               bool nativeLeft, bool defaultLeft, QObject * parent) :
    QObject(parent), loaded(false), name(name),
    slug(QString(name + DELIM + "%1" + DELIM + "%2").arg(product, 0, 16).arg(vendor, 0, 16)), 
    vendor(vendor), product(product),
    nativeLeft(nativeLeft), defaultLeft(defaultLeft),
    m_display(nullptr), m_acitve_display(m_display),
    m_left(false), m_active_left(m_left),
    m_rotation(Tablet::Rotation::Zero), m_active_rotation(m_rotation) {};

QScreen* Tablet::display() const {
    return const_cast<QScreen*>(m_display);
}

QScreen* Tablet::activeDisplay() const {
    return const_cast<QScreen*>(m_acitve_display);
}

QString Tablet::displayName() const {
    return m_display ? m_display->name() : i18n("No assigned display");
}

Matrix Tablet::calibrationMatrix() const {
    return Matrix(std::array<long double, 9> {m_calibration.a, 0, m_calibration.c,
                                0, m_calibration.e, m_calibration.f,
                                0, 0, 1}.data());
}

Matrix Tablet::mapped() const {
    CaliVars cali = m_calibration;
    if(m_display) {
        CaliVars dpy;
        QRect geo = m_display->geometry();
        qreal dpr = m_display->devicePixelRatio();
        QSize& area = ScreenMonitor::area();
        long double horizRatio = geo.width() * dpr / area.width(),
                    vertiRatio = geo.height() * dpr / area.height();
        dpy = { horizRatio, static_cast<long double>(geo.x()) / area.width(),
                vertiRatio, static_cast<long double>(geo.y()) / area.height() };
        cali.c *= horizRatio;
        cali.f *= vertiRatio;
        cali *= dpy;
    }
    if(cali.isIdentity())
        return Matrix();
    return Matrix(std::array<long double, 9> {cali.a, 0, cali.c,
                                              0, cali.e, cali.f,
                                              0, 0, 1}.data());
}

void Tablet::rotate(Matrix & matrix) const {
    switch(m_rotation) {
        case Tablet::Rotation::Zero: break;
        case Tablet::Rotation::Ninety: {
            static long double arr[] {0, -1, 1, 1, 0, 0, 0, 0, 1};
            matrix = matrix * Matrix(arr);
            break;
        }
        case Tablet::Rotation::OneEighty: {
            static long double arr[] {-1, 0, 1, 0, -1, 1, 0, 0, 1};
            matrix = matrix * Matrix(arr);
            break;
        }
        case Tablet::Rotation::TwoSeventy: {
            static long double arr[] {0, 1, 0, -1, 0, 1, 0, 0, 1};
            matrix = matrix  * Matrix(arr);
            break;
        }
    }
}

void Tablet::setDisplay(QScreen* dpy) {
    if(dpy != m_display) {
        m_display = dpy;
        emit displayChanged(dpy);
        emit displayNameChanged(displayName());
    }
}

void Tablet::findDisplay(const QString& name) {
    if(name.isEmpty()) {
        setDisplay(nullptr);
        return;
    } else if(m_display && m_display->name() == name)
        return;
    bool found = false;
    for(QScreen* dpy : QGuiApplication::screens()) {
        if(dpy->name() == name) {
            setDisplay(dpy);
            found = true;
            break;
        }
    }
    if(!found)
        qCWarning(KCM_TABLET) << "Display" << name << "not found!";
}

void Tablet::setActiveDisplay() {
    if(m_acitve_display != m_display) {
        m_acitve_display = m_display;
        emit activeDisplayChanged(m_acitve_display);
    }
}

void Tablet::setLeft(bool left) {
    if(m_left != left) {
        m_left = left;
        emit leftChanged(left);
    }
}

void Tablet::setActiveLeft() {
    if(m_active_left != m_left) {
        m_active_left = m_left;
        emit activeLeftChanged(m_active_left);
    }
}

void Tablet::setRotation(Rotation rot) {
    if(rot != m_rotation) {
        m_rotation = rot;
        emit rotationChanged(rot);
    }
}

void Tablet::setActiveRotation() {
    if(m_active_rotation != m_rotation) {
        m_active_rotation = m_rotation;
        emit activeRotationChanged(m_rotation);
    }
}

void Tablet::setCalibration(const CaliVars& vars) {
    if(m_calibration != vars) {
        m_calibration = vars;
        emit calibrationChanged(calibrationRect());
    }
}

void Tablet::setActiveCalibration() {
    m_active_calibration = m_calibration;
}

void Tablet::setButtons(QList<uint> btns) {
    if(btns != m_buttons) {
        m_buttons = btns;
        emit buttonsChanged(btns);
    }
}

void Tablet::setActive() {
    setActiveDisplay();
    setActiveLeft();
    setActiveRotation();
    setActiveCalibration();
    loaded = true;
}

void Tablet::scaleCalibration(const CaliVars& vars) {
    if(!vars.isIdentity()) {
        m_calibration = vars * m_active_calibration;
        emit calibrationChanged(calibrationRect());
    }
}

void Tablet::toggleLeft() {
    m_left = !m_left;
    emit leftChanged(m_left);
}

void Tablet::setButton(int index, uint value) {
    if(index < m_buttons.size() && m_buttons[index] != value) {
        m_buttons.replace(index, value);
        emit buttonChanged(index, value);
    }
}

QRectF Tablet::calibrationRect() const {
    return QRectF(m_calibration.c, m_calibration.f, m_calibration.a, m_calibration.e);
}
